golang-github-lib-pq (1.10.7-2+apertis1) apertis; urgency=medium

  * Switch component from development to target to comply with Apertis
    license policy.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Wed, 22 Jan 2025 14:33:53 +0100

golang-github-lib-pq (1.10.7-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 06 Apr 2023 10:44:35 +0000

golang-github-lib-pq (1.10.7-2) unstable; urgency=medium

  * Minor autopkgtest refinements
  * Reinstate previously excluded auth/kerberos package
  * Add dependency golang-github-jcmturner-gokrb5.v8-dev

 -- Daniel Swarbrick <dswarbrick@debian.org>  Fri, 09 Dec 2022 08:18:38 +0000

golang-github-lib-pq (1.10.7-1) unstable; urgency=medium

  * New upstream release

 -- Daniel Swarbrick <dswarbrick@debian.org>  Sun, 18 Sep 2022 15:50:00 +0000

golang-github-lib-pq (1.10.6-4) unstable; urgency=medium

  * autopkgtest: disable go vet and _really_ fix autopkgtest (Closes: #970788)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Fri, 16 Sep 2022 01:19:09 +0000

golang-github-lib-pq (1.10.6-3) unstable; urgency=medium

  * autopkgtest: add gcc dependency, which is needed due to this package's
    import of the net package, and the fact that Go uses the libc DNS resolver
    by default, unless CGO_ENABLED=0.
  * autopkgtest: replace golang-go dependency with golang-any, as per
    Build-Depends.

 -- Daniel Swarbrick <dswarbrick@debian.org>  Thu, 15 Sep 2022 12:42:50 +0000

golang-github-lib-pq (1.10.6-2) unstable; urgency=medium

  * Fix failing autopkgtest
    - reload Postgres after setting authentication config
    - run go test as unprivileged user to avoid a specific test failure
  * Add new 01-fix-CloseBadConn-test.patch to support Unix sockets

 -- Daniel Swarbrick <dswarbrick@debian.org>  Thu, 15 Sep 2022 00:01:56 +0000

golang-github-lib-pq (1.10.6-1) unstable; urgency=medium

  [ Daniel Swarbrick ]
  * New upstream release
  * Bump Standards-Version to 4.6.1 (no changes)
  * d/rules: don't build optional kerberos auth support

  [ Tianon Gravi ]
  * Remove self from Uploaders

 -- Daniel Swarbrick <dswarbrick@debian.org>  Mon, 05 Sep 2022 17:10:34 +0000

golang-github-lib-pq (1.5.2-1) unstable; urgency=medium

  [ Aloïs Micard ]
  * Update debian/gitlab-ci.yml (using pkg-go-tools/ci-config)

  [ Daniel Swarbrick ]
  * New(er) upstream release
  * Drop transitional package golang-pq-dev (Closes: #939216)
  * Mark as Multi-Arch: foreign

  [ Guillem Jover ]
  * Update gbp.conf following latest Go Team workflow
  * Update debian/watch file
  * Remove README.Source, covered by debian/copyright
  * Use dh-sequence-golang instead of dh-golang and --with=golang
  * Move license location on Debian systems into a Comment field
  * Remove ${shlibs:Depends} from -dev Depends field
  * Do not install uninteresting README.md
  * Switch to Standards-Version 4.6.0 (no changes needed)
  * Switch to debhelper-compat 13

 -- Daniel Swarbrick <dswarbrick@debian.org>  Thu, 09 Dec 2021 13:55:35 +0000

golang-github-lib-pq (1.3.0-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 16:45:22 +0000

golang-github-lib-pq (1.3.0-1) unstable; urgency=medium

  [ Daniel Swarbrick ]
  * New upstream release.
  * Add myself to uploaders.
  * Update Standards-Version to 4.4.1 (no changes).
  * Update debhelper version to 12.
  * d/gbp.conf: stop using pristine-tar.
  * Add debian/watch file.

 -- Daniel Swarbrick <daniel.swarbrick@cloud.ionos.com>  Fri, 17 Jan 2020 13:05:51 +0000

golang-github-lib-pq (0.0~git20151007.0.ffe986a-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 12:01:28 +0000

golang-github-lib-pq (0.0~git20151007.0.ffe986a-2) unstable; urgency=medium

  [ Paul Tagliamonte ]
  * Team upload.
  * Use a secure transport for the Vcs-Git and Vcs-Browser URL
  * Remove Built-Using from arch:all -dev package

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Dr. Tobias Quathamer ]
  * Use debhelper v11
  * Use golang-any instead of golang-go
  * Update team name and address
  * Update to Standards-Version 4.2.1
    - Use HTTPS URL for d/copyright
    - Use Priority: optional

 -- Dr. Tobias Quathamer <toddy@debian.org>  Thu, 29 Nov 2018 21:38:51 +0100

golang-github-lib-pq (0.0~git20151007.0.ffe986a-1) unstable; urgency=medium

  * Update to newer upstream snapshot (Closes: #801807)
  * Bring package under the pkg-go umbrella
  * Update packaging to use dh-golang instead of being totally ad-hoc
  * Rename source/binary packages in line with pkg-go policy
  * Add DEP8 test to run the package tests (which require a running Postgres
    instance)

 -- Tianon Gravi <tianon@debian.org>  Thu, 15 Oct 2015 14:28:28 -0700

golang-pq-dev (0.0~git20130606-1) unstable; urgency=low

  * Initial release (Closes: #714451)

 -- Michael Stapelberg <stapelberg@debian.org>  Sat, 29 Jun 2013 16:19:25 +0200
